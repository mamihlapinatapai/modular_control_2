﻿#include <stdio.h>
#include <stdlib.h>
#include <Windows.h>


#define n 10
#define col_razr 3


int velich_razr(int chislo, int razr)
{
	while (razr > 1)
	{
		chislo /= 10;
		razr--;
	}
	return chislo % 10;
}

void sort_razr(int new_arr[n][n], int arr[n], int razr)
{
	int arr_col[n], i, j, temp = 0;
	for (i = 0; i < n; i++)
		arr_col[i] = 0;
	for (i = 0; i < n; i++)
	{
		int a = velich_razr(arr[i], razr);
		new_arr[arr_col[a]][a] = arr[i];
		arr_col[a]++;
	}
	for (i = 0; i < n; i++)
	{
		for (j = 0; j < arr_col[i]; j++)
		{
			arr[temp] = new_arr[j][i];
			temp++;
		}
	}
}

int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);

	int razr, i;
	int arr[n] = { 100, 97, 3, 28, 15, 17, 6, 127, 11, 102 };
	int new_arr[n][n];

	printf("Є масив до сортування: \n\n");
	for (i = 0; i < n; i++)
		printf("%d\t", arr[i]);

	printf("\n\n");

	printf("Після сортування: \n");
	for (razr = 1; razr < 4; razr++)
		sort_razr(new_arr, arr, razr);

	printf("\n\n");

	for (i = 0; i < n; i++)
		printf("%d\t", arr[i]);

	printf("\n\n");
	return 0;
}
